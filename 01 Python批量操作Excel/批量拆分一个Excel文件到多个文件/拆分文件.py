
import pandas as pd

df = pd.read_excel("产品统计表.xlsx")

products = df[["单号", "产品名称"]].unique()

for number, name in products:
    df_product = df[(df["单号"] == number)
                    & (df["产品名称"] == name)]
    df_product.to_excel(
        f"{number}-{name}.xlsx", index=False)
