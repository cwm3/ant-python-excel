
datas = [
    ('销售部', '张大拿'),
    ('技术部', '王大强'),
    ('运营部', '刘大彪'),
    ('财务部', '李大梅')
]

import shutil
import xlwings as xw

app = xw.App(visible=False, add_book=False)

for dept, manager in datas:
    target_excel = f"部门业绩-{dept}.xlsx"
    shutil.copy("部门业绩-模板.xlsx", target_excel)
    workbook = app.books.open(target_excel)
    worksheet = workbook.sheets[0]
    worksheet['A1'].value = worksheet['A1'].value.replace("{dept}", dept)
    worksheet['A2'].value = worksheet['A2'].value.replace("{manager}", manager)
    workbook.save()
    workbook.close()

app.quit()
