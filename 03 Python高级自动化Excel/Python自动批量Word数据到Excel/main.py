
from docx import Document
import pandas as pd
import os
def parse_docfile(doc_file):
    doc = Document(doc_file)
    table = doc.tables[0]
    return dict(
        姓名 = table.cell(0, 1).text, 性别 = table.cell(0, 3).text,
        民族 = table.cell(0, 5).text, 出生年月 = table.cell(0, 7).text,

        参加工作时间 = table.cell(1, 1).text, 学历 = table.cell(1, 3).text,
        籍贯 = table.cell(1, 5).text, 政治面貌 = table.cell(1, 7).text,

        毕业院校 = table.cell(2, 1).text, 专业 = table.cell(2, 4).text,
        职务 = table.cell(2, 7).text,

        工作单位 = table.cell(3, 1).text, 报考类别 = table.cell(3, 7).text,

        工作简历 = table.cell(4, 1).text.strip(),
        工作业绩 = table.cell(5, 1).text.strip(),
        单位推荐意见 = table.cell(6, 1).text.strip(),
        领导意见 = table.cell(7, 1).text.strip(),
        )
# 列名
columns = None
# 数据内容
datas = []
for file in os.listdir("优秀教师选拔考试报名表"):
    if file.endswith(".docx"):
        file_path = f"优秀教师选拔考试报名表/{file}"

        print("解析文件", file_path)

        data = parse_docfile(file_path)

        if not columns:
            columns = list(data.keys())

        datas.append([data[column] for column in columns])

df= pd.DataFrame(datas, columns=columns)

df.to_excel("优秀教师选拔考试报名表.xlsx",index=False)