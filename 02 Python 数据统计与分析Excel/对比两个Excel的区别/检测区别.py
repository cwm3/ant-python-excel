import xlwings as xw

app = xw.App(visible=False, add_book=False)
book = app.books.open("期末考试-学生成绩表.xlsx")
book_backup = app.books.open("期末考试-学生成绩表 - 备份.xlsx")

for row in book.sheets[0].range("A1").expand():
    for cell in row:
        backup_cell = book_backup.sheets[0].range(cell.address)
        if cell.value != backup_cell.value:
            cell.color = backup_cell.color = (255, 0, 0)
book.save()
book.close()
book_backup.save()
book_backup.close()
app.quit()